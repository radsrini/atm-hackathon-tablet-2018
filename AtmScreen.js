import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Picker,
  TextInput,
} from 'react-native';
import Modal from 'react-native-modal'; // Version can be specified in package.json
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import { LinearGradient } from 'expo';

export class AtmScreen extends Component {
  constructor() {
    super();
    this.state = {
      visibleModal: null,
      amount: '1000',
      recipientOptionValue: 0,
      selectedIdentification: 0,
      SSNText: '',
      aadharText: '',
      email: '',
      amountWithdrawn: '1000',
      password: 'Password',
    };
  }

  _renderImage = (img, imageTitle, onPress) => (
    <TouchableOpacity onPress={onPress} style={styles.imageWrapper}>
      <Image source={img} />
      <Text style={styles.imgText}>
        {imageTitle}
      </Text>
    </TouchableOpacity>
  );

  _renderButton = (text, onPress) => (
    <TouchableHighlight onPress={onPress} style={styles.button} underlayColor='#F7B600'>
        <Text style={styles.buttonText}>{text}</Text>
    </TouchableHighlight>
  );

  _renderModalContent_1 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>CASH DEPOSIT</Text>
      <View style={styles.modelContentView}>
        <Text style={{marginTop: 15, fontSize: 20}}>Amount: </Text>
        <Picker
          selectedValue={this.state.amount}
          style={{ height: 80, width: 400 }} 
          itemStyle={{fontSize: 20, textAlign: 'center'}} 
          onValueChange={(itemValue, itemIndex) =>
            this.setState({ amount: itemValue })
          }>
          <Picker.Item label="50" value="50" />
          <Picker.Item label="100" value="100" />
          <Picker.Item label="500" value="500" />
          <Picker.Item label="1000" value="1000" />
        </Picker>
      </View>
      <View style={styles.modelContentView}>
      <Text style={{marginTop: 10, fontSize: 20}}>SSN:     </Text>
      <TextInput
            style={styles.cashDepositIdentityText}
            onChangeText={text => this.setState({ SSNText: text })}
            value={this.state.SSNText} 
            underlineColorAndroid = "#ffffff"
          />
      </View>
      <View style={styles.modelContentView}>
      <Text style={{marginTop: 10, fontSize: 20}}>Aadhar: </Text>
      <TextInput
            style={styles.cashDepositIdentityText}
            onChangeText={text => this.setState({ aadharText: text })}
            value={this.state.aadharText} 
            underlineColorAndroid = "#ffffff"
          />
      </View>
      <View style={styles.modelContentView}>
      <Text style={{marginTop: 10, fontSize: 20}}>Email:   </Text>
      <TextInput
            style={styles.cashDepositIdentityText} 
            onChangeText={text => this.setState({ email: text })}
            value={this.state.email} 
            underlineColorAndroid = "#ffffff"
          />
      </View>
      <View style={{ flex: 1, flexDirection: 'row'}}>
        {this._renderButton('Deposit', () =>
          this.setState({ visibleModal: null })
        )}
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );

  _renderModalContent_2 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>CASH DEPOSIT</Text>
      <Image source={require('./check_mark_gold.png')} style={styles.checkMark} />
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {this._renderButton('Fund Transfer', () =>
          this.setState({ visibleModal: null })
        )}
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );


 _renderModalContent_3 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>FUND TRANSFER</Text>
      <View style={styles.modelContentView}>
      <Text style={{marginTop: 10, fontSize: 20}}>Amount: </Text>
        <TextInput
            style={styles.cashDepositIdentityText} 
            editable={false}
            value={this.state.amount} 
            underlineColorAndroid = "#ffffff"
          />
      </View>
      <View style={{ flex: 1, flexDirection: 'row', position: 'absolute',bottom: 160 }}>
        {this._renderButton('Continue', () =>
          this.setState({ visibleModal: null })
        )}
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );

  _renderModalContent_4 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>FUND TRANSFER</Text>
      <Text style={{ fontSize: 25, marginTop: 15,fontFamily: 'Georgia', color: 'grey' }}>
        Recipient details
      </Text>
      <View style={{flex: 1, marginTop: 15}}>
     <RadioForm
radio_props={radio_props}
initial={0}
onPress={value => {
this.setState({ value: value, selectedIdentification: 1 });
}}
style={{alignItems: 'flex-start'}}
buttonColor={'#F7B600'}
selectButtonColor={'#F7B600'}
buttonSize={10}
/>
      </View>
      <View>
        {this.state.selectedIdentification !== -1 ? (
          <TextInput
            style={styles.cashDepositIdentityText} 
            onChangeText={text => this.setState({ text: text })}
            value={this.state.text} 
            underlineColorAndroid = "#ffffff"
          />
        ) : null}
      </View>
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {this._renderButton('Continue', () =>
          this.setState({ visibleModal: null })
        )}
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );

  _renderModalContent_5 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>FUND TRANSFER</Text>
      <Image source={require('./check_mark_gold.png')} style={styles.checkMark} />
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );

  _renderModalContent_6 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>CASH WITHDRAWAL</Text>
      <View style={styles.modelContentView}>
        <Text style={{marginTop: 15, fontSize: 20}}>Amount: </Text>
        <Picker
          selectedValue={this.state.amountWithdrawn}
          style={{ height: 80, width: 400 }} 
          itemStyle={{fontSize: 20, textAlign: 'center'}} 
          onValueChange={(itemValue, itemIndex) =>
            this.setState({ amountWithdrawn: itemValue })
          }>
          <Picker.Item label="50" value="50" />
          <Picker.Item label="100" value="100" />
          <Picker.Item label="500" value="500" />
          <Picker.Item label="1000" value="1000" />
        </Picker>
      </View>
      <View style={{ flex: 1, flexDirection: 'row'}}>
        {this._renderButton('Withdraw', () =>
          this.setState({ visibleModal: null })
        )}
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );

    _renderModalContent_7 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>CASH WITHDRAWAL</Text>
      <View>
          <TextInput
            style={styles.cashWithdrawIdentityText} 
            onChangeText={password => this.setState({ password: password })}
            value={this.state.password} 
            underlineColorAndroid = "#ffffff"
          />
          <TouchableOpacity onPress={() => this.setState({ visibleModal: null })} style={styles.imageWrapper}>
          <Image source={require('./Fingerprint.png')} style={styles.fingerprint} /> 
          </TouchableOpacity>
      </View>
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );

  _renderModalContent_8 = () => (
    <View style={styles.modalInnerWrapper}>
      <Text style={styles.modalTitle}>CASH WITHDRAWAL</Text>
      <Image source={require('./check_mark_gold.png')} style={styles.checkMark} />
      <View style={{ flex: 1, flexDirection: 'row' }}>
        {this._renderButton('Close', () =>
          this.setState({ visibleModal: null })
        )}
      </View>
    </View>
  );



  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#1A1F71'}}>
      <LinearGradient
          colors={['rgba(0,0,0,0.8)', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 300,
          }}
        />
        <View style={styles.topContainer}>
          <Image source={require('./atm_screens_assets_v4_tablet_logo-01.png')} style={{width: 280, height: 80}} />
        </View>
        <View style={styles.imgcontainer}>
          {this._renderImage(require('./cashdeposit.png'), 'Cash Deposit', () =>
            this.setState({ visibleModal: 1 })
          )}
          {this._renderImage(require('./inquiry.png'), 'Inquiry', () =>
            this.setState({ visibleModal: null})
          )}
          {this._renderImage(require('./cashwithdrawal.png'), 'Cash Withdrawal', () =>
            this.setState({ visibleModal: 6 })
          )}
          {this._renderImage(require('./billpayment.png'), 'Bill Payment', () =>
            this.setState({ visibleModal: null })
          )}
          {this._renderImage(require('./transfer.png'), 'Transfer', () =>
            this.setState({ visibleModal: 2 })
          )}
          {this._renderImage(require('./location.png'), 'ATM Locator', () =>
            this.setState({ visibleModal: null })
          )}
        </View>
        <View style={styles.modalView}>
           <Modal isVisible={this.state.visibleModal === 1} 
           onModalHide={() => {this.setState({ visibleModal: 2 });}} 
          onRequestClose={() => {
          }}  >
            {this._renderModalContent_1()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 2}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'} 
            onModalHide={() => {this.setState({ visibleModal: 3 });}} 
            onRequestClose={() => {
          }} >
            {this._renderModalContent_2()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 3}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'} 
            onModalHide={() => {this.setState({ visibleModal: 4 });}} 
            onRequestClose={() => {
          }} >
            {this._renderModalContent_3()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 4}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'} 
            onModalHide={() => {this.setState({ visibleModal: 5 });}} 
            onRequestClose={() => {
          }} >
            {this._renderModalContent_4()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 5}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'} 
            onModalHide={() => {this.setState({ visibleModal: null });}} 
            onRequestClose={() => {
          }} >
            {this._renderModalContent_5()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 6}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'}
            onModalHide={() => {this.setState({ visibleModal: 7 });}} 
            onRequestClose={() => {
          }} >
            {this._renderModalContent_6()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 7}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'}
            onModalHide={() => {this.setState({ visibleModal: 8 });}} 
            onRequestClose={() => {
          }} >
            {this._renderModalContent_7()}
          </Modal>
          <Modal
            isVisible={this.state.visibleModal === 8}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'}
            onModalHide={() => {this.setState({ visibleModal: null });}} 
            onRequestClose={() => {
          }} >
            {this._renderModalContent_8()}
          </Modal>
        </View>
      </View>
    );
  }
}

const radio_props = [
  { label: 'Mobile Phone (10-digit #)', value: 0 },
  { label: 'Email', value: 1 },
];

const styles = StyleSheet.create({
  
  // topContainer: {
  //   height: 60,
  //   flexDirection: 'column',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   margin: 10,
  // },

  topContainer: {
    height: 150,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 10
  },

  imgcontainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',  
  },

  // imageWrapper: {
  //   width: 133,
  //   height: 133,
  //   margin: 10,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#eee',
  //   borderRadius: 4
    
  // },

  imageWrapper: {
    width: 250,
    height: 250,
    margin: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    borderRadius: 8
    
  },

  // imgText: { 
  //   marginTop: 10, 
  //   color: 'grey', 
  //   justifyContent: 'center' 
  //   },

  imgText: { 
    fontSize: 20,
    color: 'grey',
    marginTop: 20, 
    justifyContent: 'center' 
    },

  // checkMark: {
  //   width: 80,
  //   height: 60,
  //   margin: 20,
  // },

  checkMark: {
    width: 200,
    height: 160,
    margin: 40,
  },

  fingerprint: {
    width: 200,
    height: 200,
    margin: 40,
  },

  // button: {
  //   backgroundColor: '#003EA9',
  //   padding: 12,
  //   margin: 2,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   borderRadius: 4,
  //   borderColor: 'rgba(0, 0, 0, 0.1)',
  //   width: 185,
  //   height: 40
  // },

  button: {
    backgroundColor: '#003EA9',
    padding: 20,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 10,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    width: 350,
    height: 60
  },

//only for tab
  buttonText: { 
    color: 'white',
    fontSize: 20
  },

  // modalView: { flex: 1, width: 60, height: 60 },
  modalView: { flex: 1, width: 350, height: 350  },

  // modalInnerWrapper: {
  //   backgroundColor: 'white',
  //   padding: 22,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   borderRadius: 4,
  //   borderColor: 'rgba(0, 0, 0, 0.1)',
  //   height: 420,
  // },

  modalInnerWrapper: {
    backgroundColor: 'white',
    padding: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    height: 650,
  },


  // modelContentView: { 
  //   flexDirection: 'row', 
  //   marginTop: 10
  //   },

    modelContentView: { 
    flexDirection: 'row', 
    marginTop: 20
    },

  // modalTitle: {
  //   fontSize: 15,
  //   fontFamily: 'Georgia',
  //   color: '#ddd',
  // },

 modalTitle: {
    fontSize: 30,
    fontFamily: 'Georgia',
    color: '#bbb',
  },

  // cashDepositIdentityText: {
  //   margin: 5,
  //   height: 40,
  //   borderColor: '#F7B600',
  //   borderWidth: 1,
  //   width: 200,
  // },

  cashDepositIdentityText: {
    margin: 15,
    height: 80,
    borderColor: '#F7B600',
    borderWidth: 1,
    width: 400,
    fontSize: 30
  },

  cashWithdrawIdentityText: {
    margin: 15,
    height: 80,
    borderColor: '#F7B600',
    borderWidth: 1,
    width: 400,
    fontSize: 30,
    color: '#cccccc'
  },

});