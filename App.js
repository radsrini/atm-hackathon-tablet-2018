import React from 'react';
import { View, Text } from 'react-native';
import { StackNavigator, navigationOptions } from 'react-navigation';
import { HomeScreen } from './HomeScreen';
import { DetailsScreen } from './Detail';
import { AtmScreen } from './AtmScreen';


const RootStack = StackNavigator(
  {
    Home: { screen: HomeScreen,
          navigationOptions: 
          { header: null }
        },
    Details: { screen: DetailsScreen },
    Atm: { screen: AtmScreen,
    navigationOptions: 
    {header: null }
  },
    },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
