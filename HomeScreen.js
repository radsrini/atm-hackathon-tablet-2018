import React from 'react';
import { View, Text, Button, Image, TouchableOpacity } from 'react-native';
import { StackNavigator } from 'react-navigation';

export class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <TouchableOpacity onPress={() => this.props.navigation.navigate('Atm')}>
      <Image source={require('./atm_intro_screen_phone-01.png')} style={{width: 800, height: 1280}} />
    </TouchableOpacity>
      </View>
    );
  }
}

//width: 231, 
//height: 640,